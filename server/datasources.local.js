var mongodburl = (process.env.OPENSHIFT_MONGODB_DB_URL || "mongodb://localhost:27017/") + "buddysworldapi";
module.exports = {
  "memorydb": {
    "database": "BuddysWorldDatabase",
    "password": "",
    "name": "memorydb",
    "user": "",
    "file": "server/memory_database.json",
    "connector": "memory"
  },
  "mongodb": {
    "url": mongodburl,
    "name": "mongodb",
    "connector": "mongodb",
    "allowExtendedOperators": true,
    "enableGeoIndexing": true
  }
}
