"use strict";

module.exports = function(server) {

    const AccessToken = server.models.AccessToken;
    const Person = server.models.Person;
    const router = server.loopback.Router();

    router.post('/api/people/updateMe', function(req, res, next) {

        const accessTokenId = req.body.accessToken;
        if (accessTokenId) {
            AccessToken.findById(accessTokenId, function(err, accessToken) {
                if (err) res.send(returnErrorObject(err));
                else Person.findById(accessToken.userId, function(err, person) {
            
                    // delete req.body.id;
                    if (err) res.send(returnErrorObject(err));
                    else person.updateAttributes(req.body, function(err, personUpdated) {

                        if (err) res.send(returnErrorObject(err));
                        else {
                            delete personUpdated.password;
                            res.send(personUpdated);
                        }
                    });
                });
            });
        }
        else {
            res.send(returnErrorObject("Authorization required!"));
        }
    });

    server.use(router);
}


function returnErrorObject(error) {
    return {"name" : "update", "status" : 404, "message" : error};
}