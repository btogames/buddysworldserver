"use strict";

const MongoClient = require('mongodb').MongoClient;
const mongoURL = (process.env.OPENSHIFT_MONGODB_DB_URL || "mongodb://localhost:27017/") + "buddysworldapi";
const async = require('async');

module.exports = function(app, andCallItWhenAllIndexationsAreDone) {

    MongoClient.connect(mongoURL, runEnsureIndexes);

    function runEnsureIndexes(connectError, db) {
        ensureIndexesOn(db, ['Person']);
    }

    function ensureIndexesOn(db, collectionsNames) {

        async.each(collectionsNames, ensureIndexOf, whenAllEnsuranceIsDone);

        function ensureIndexOf(collectionName, andCallItWhenIndexationDone) {
            let collection = db.collection(collectionName);
            collection.ensureIndex({ location: '2dsphere' }, function () {
                andCallItWhenIndexationDone();
            });
        }

        function whenAllEnsuranceIsDone(error) {
            andCallItWhenAllIndexationsAreDone();
        }
    }
}