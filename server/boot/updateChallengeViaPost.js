"use strict";

const FINISHED = "FINISHED";
const ACCEPTED = "ACCEPTED";
const REJECTED = "REJECTED";
const INCLUDE_CHA = {include : ["challenger", "challenged" ]};

module.exports = function (server) {

    const AccessToken = server.models.AccessToken;
    const Challenge = server.models.Challenge;
    const Person = server.models.Person;
    const router = server.loopback.Router();

    router.post("/api/challenges/update", function (req, res) {

        const accessTokenId = req.body.accessToken;
        if (accessTokenId) {
            AccessToken.findById(accessTokenId, function (err, accessToken) {
                if (err) res.send(returnErrorObject(err));
                else Person.findById(accessToken.userId, function (err, person) {
                    const challengeId = req.body.id;
                    
                    if (challengeId) {
                        Challenge.findById(challengeId, INCLUDE_CHA, function (err, challenge) {

                            if (err) returnErrorObject(err);
                            else {
                                let updateObject = JSON.parse(JSON.stringify(req.body));

                                if (updateObject.status === REJECTED) {
                                    Challenge.destroyById(challengeId, function(err, challengeDeleted) {
                                        if (err) returnErrorObject(err);
                                        else {
                                            res.send(challengeDeleted);
                                        }
                                    })
                                }
                                else {

                                    if (person.id === challenge.challenger.id) {
                                        if (challenge.challengerMetersRan >= challenge.meters) {
                                            updateObject.status = FINISHED;
                                        }
                                    }
                                    else {
                                        if (challenge.challengedMetersRan >= challenge.meters) {
                                            updateObject.status = FINISHED;    
                                        }
                                    }

                                    challenge.updateAttributes(updateObject, INCLUDE_CHA, function (err, challengeUpdated) {
                                        if (err) returnErrorObject(err);
                                        else {

                                            res.send(challengeUpdated);
                                        }
                                    });
                                }
                            }
                        });
                    }
                    else returnErrorObject("No id provided!");
                });
            })
        }
        else returnErrorObject("No access token provided!");


    });

    router.post("/api/challenges/create", function(req, res) {
        console.log(req.body);
        Challenge.create(req.body, function(err, challenge) {
            console.log(challenge);
            if (err) returnErrorObject(err);
            else res.send(challenge);
        })
    })

    server.use(router);
};


function returnErrorObject(error) {
    return { "name": "update", "status": 404, "message": error };
}