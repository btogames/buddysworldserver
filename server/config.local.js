var host = process.env.OPENSHIFT_NODEJS_IP || "localhost";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

console.log(host, port);

module.exports = {
  "restApiRoot": "/api",
  "host": host,
  "port": port,
  "remoting": {
    "context": false,
    "rest": {
      "normalizeHttpPath": false,
      "xml": false
    },
    "json": {
      "strict": false,
      "limit": "1gb"
    },
    "urlencoded": {
      "extended": true,
      "limit": "1gb"
    },
    "cors": false,
    "handleErrors": false
  },
  "legacyExplorer": false
}
