const loopback = require('loopback');
const app = require('../../server/server');
const GeoPoint = loopback.GeoPoint;

module.exports = function(Person) {

    Person.around = function(lat, lng, maxDistance, unit, returnPeople) {
        var currentLocation = new GeoPoint({ lat: lat, lng: lng });

        var locationQuery = {
            where: {
                location: {
                    near: currentLocation,
                    maxDistance: maxDistance,
                    unit: unit
                }
            }
        };

        Person.find(locationQuery, returnPeople);
    };

    Person.remoteMethod(
        'around',
        {
            accepts: [
                { arg: 'lat', type: 'number', required: true },
                { arg: 'lng', type: 'number', required: true },
                { arg: 'maxDistance', type: 'number', required: false, default: 2000 },
                { arg: 'unit', type: 'string', required: false, default: 'meters' }
            ],
            http: { path: '/around', verb: 'get' },
            returns: { arg: 'data', type: ['Person'], root: true }
        }
    )


    Person.ranking = function(limit, returnRank) {
        Person.find({order : ['metersRan DESC'], limit : limit}, returnRank);
    }

    Person.remoteMethod(
        'ranking',
        {
            accepts : [{arg:'limit', type:'number', default:10, required:false}],
            http : { path:'/ranking', verb:'get'},
            returns : {arg:'data', type:['Person'], root:true}
        }
    )
}